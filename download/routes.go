package download

import (
	"net/http"
	"time"

	"github.com/didip/tollbooth"
	"github.com/didip/tollbooth/limiter"
	"github.com/didip/tollbooth_chi"
	"github.com/go-chi/chi"
)

func Routes() chi.Router {
	lmt := tollbooth.NewLimiter(1, &limiter.ExpirableOptions{DefaultExpirationTTL: time.Second * 10})
	lmt.SetMethods([]string{"GET", "POST"})
	lmt.SetMessage("You are limited to one request per 10 seconds.")
	lmt.SetOnLimitReached(func(w http.ResponseWriter, r *http.Request) {
		redirectToIndexWithFlashMsg(w, r, "You're limited to 1 request per 10 seconds.")
	})

	r := chi.NewRouter()

	r.Use(tollbooth_chi.LimitHandler(lmt))

	r.Get("/{id}", download)
	r.Post("/", download)

	return r
}
