package download

import (
	"fmt"
	"net/http"
	"os/exec"
	"strings"

	"gitlab.com/maike.me/youtube-dlmp3.com/handlers"
)

func updateYouTubeDL() error {
	var err error
	cmd := "youtube-dl"
	arg := "-U"
	if err = exec.Command(cmd, arg).Run(); err != nil {
		return err
	}
	return nil
}

func extractYouTubeID(url string) string {
	if len(url) == 11 {
		return url
	}
	var id string
	var urlSplit []string
	switch {
	case strings.Contains(url, "youtu.be/"):
		urlSplit = strings.Split(url, "youtu.be/")
	case strings.Contains(url, "youtube.com"):
		switch {
		case strings.Contains(url, ".com/v/"):
			urlSplit = strings.Split(url, ".com/v/")
		case strings.Contains(url, ".com/e/"):
			urlSplit = strings.Split(url, ".com/e/")
		case strings.Contains(url, ".com/embed/"):
			urlSplit = strings.Split(url, ".com/embed/")
		case strings.Contains(url, ".com/watch?v="):
			urlSplit = strings.Split(url, ".com/watch?v=")
		case strings.Contains(url, "player_embedded&v="):
			urlSplit = strings.Split(url, "player_embedded&v=")
		default:
			return ""
		}
	default:
		return ""
	}
	if len(urlSplit[1]) >= 11 {
		id = urlSplit[1][:11]
	}
	return id
}

func redirectToIndexWithFlashMsg(w http.ResponseWriter, r *http.Request, message string) {
	sess := handlers.GetSession(r)
	sess.AddFlash(message)
	err := sess.Save(r, w)
	if err != nil {
		fmt.Println(err)
	}
	http.Redirect(w, r, "/", http.StatusSeeOther)
}
