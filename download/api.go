package download

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"time"

	"github.com/go-chi/chi"
)

func download(w http.ResponseWriter, r *http.Request) {
	var id string
	switch r.Method {
	case "GET":
		id = chi.URLParam(r, "id")
	case "POST":
		id = extractYouTubeID(r.PostFormValue("url"))
	}
	if id == "" {
		redirectToIndexWithFlashMsg(w, r, fmt.Sprintf("YouTube ID is invalid."))
		return
	}
	cmd := "youtube-dl"
	args := []string{"--extract-audio", "--audio-format", "mp3", "--audio-quality", "0", "--prefer-avconv", "--output", "%(id)s.%(ext)s", fmt.Sprintf("https://youtube.com/watch?v=%s", id)}
	if err := exec.Command(cmd, args...).Run(); err != nil {
		if err = updateYouTubeDL(); err != nil {
			redirectToIndexWithFlashMsg(w, r, fmt.Sprintf("Failed to update YouTube-DL, please let me know at support@youtube-dlmp3.com. Error: %s", err))
			return
		}
		redirectToIndexWithFlashMsg(w, r, fmt.Sprintf("Failed to download YouTube file, error: %s.", err))
		return
	}
	mp3File, err := ioutil.ReadFile(id + ".mp3")
	if err != nil {
		redirectToIndexWithFlashMsg(w, r, fmt.Sprintf("Failed to read MP3 file, error: %s.", err))
		return
	}
	err = os.Remove(id + ".mp3")
	if err != nil {
		redirectToIndexWithFlashMsg(w, r, fmt.Sprintf("Failed to delete MP3 file, error: %s.", err))
		return
	}

	currentDir, _ := os.Getwd()
	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Disposition", "attachment; filename=\""+id+".mp3"+"\"")
	w.Header().Set("Content-Transfer-Encoding", "binary")
	w.Header().Set("Expires", "0")
	http.ServeContent(w, r, currentDir, time.Now(), bytes.NewReader(mp3File))
}
