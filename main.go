package main

import (
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/gorilla/csrf"
	"gitlab.com/maike.me/youtube-dlmp3.com/config"
	"gitlab.com/maike.me/youtube-dlmp3.com/download"
	"gitlab.com/maike.me/youtube-dlmp3.com/templates"
)

func main() {
	r := chi.NewRouter()

	r.Use(middleware.Recoverer)

	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300,
	})
	r.Use(cors.Handler)

	r.Get("/", indexGET)
	r.Mount("/download", download.Routes())

	var csrfAuthKey string
	var csrfSecure bool
	switch config.RuntimeViper().GetString("environment") {
	case "development":
		csrfAuthKey = config.RuntimeViper().GetString("csrf.development.authenticationKey")
		csrfSecure = false
	case "production":
		csrfAuthKey = config.RuntimeViper().GetString("csrf.production.authenticationKey")
		csrfSecure = true
	default:
		panic("Environment variable not set")
	}
	CSRF := csrf.Protect(
		[]byte(csrfAuthKey),
		csrf.RequestHeader("Authenticity-Token"),
		csrf.FieldName("authenticity_token"),
		csrf.Secure(csrfSecure),
		csrf.Path("/"),
	)

	currentDir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	filesDir := filepath.Join(currentDir, "static")
	FileServer(r, "/static", http.Dir(filesDir))

	panic(http.ListenAndServe(config.RuntimeViper().GetString("port"), CSRF(r)))
}

func indexGET(w http.ResponseWriter, r *http.Request) {
	commonData := templates.ReadCommonData(w, r)
	templates.Render(w, "index.html", map[string]interface{}{
		"Common": commonData,
	})
}

func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}
	fs := http.StripPrefix(path, http.FileServer(root))
	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"
	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	}))
}
