package handlers

import (
	"net/http"

	"github.com/gorilla/sessions"
	"gitlab.com/maike.me/youtube-dlmp3.com/config"
)

var sessionStore *sessions.CookieStore

func init() {
	switch config.RuntimeViper().GetString("environment") {
	case "development":
		sessionStore = sessions.NewCookieStore(
			[]byte(config.RuntimeViper().GetString("session.development.authenticationKey")),
			[]byte(config.RuntimeViper().GetString("session.development.encryptionKey")))
		sessionStore.Options = &sessions.Options{
			Domain: config.RuntimeViper().GetString("session.development.domain"),
			Path:   "/",
			MaxAge: 28800,
		}
	case "production":
		sessionStore = sessions.NewCookieStore(
			[]byte(config.RuntimeViper().GetString("session.production.authenticationKey")),
			[]byte(config.RuntimeViper().GetString("session.production.encryptionKey")))
		sessionStore.Options = &sessions.Options{
			Domain: config.RuntimeViper().GetString("session.production.domain"),
			Path:   "/",
			MaxAge: 28800,
		}
	default:
		panic("environment variable not set")
	}
}

func GetSession(r *http.Request) *sessions.Session {
	session, _ := sessionStore.Get(r, "youtube-dlmp3-session")
	return session
}
