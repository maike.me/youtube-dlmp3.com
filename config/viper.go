package config

import "github.com/spf13/viper"

var runtimeViper *viper.Viper

func RuntimeViper() *viper.Viper {
	if runtimeViper == nil {
		runtimeViper = viper.New()
		runtimeViper.SetConfigName("config")
		runtimeViper.SetConfigType("json")
		runtimeViper.AddConfigPath("./")
		err := runtimeViper.ReadInConfig()
		if err != nil {
			panic(err)
		}
	}
	return runtimeViper
}
