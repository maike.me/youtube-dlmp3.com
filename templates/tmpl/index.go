package tmpl

const IndexHTML = `
{{ define "content" }}
<div>
<form action="/download" method="post" style="text-align: center;">
	<input type="text" name="url" id="url" placeholder="https://youtube.com/watch?v=1XXXXXXXX11" style="text-align: center; color: black; height: 2rem; width: 23rem;">
	<br>
	{{ .Common.CSRF }}
	<input type="submit" id="download" value="download" style="color: black; margin-top: .75rem;" onclick="this.disabled=true;this.value='downloading..';this.form.submit();setTimeout('enable()', 10000);">
</form>
</div>
{{ end }}
{{ define "scripts" }}
<script type="text/javascript">
	function enable() {
		var btn = document.getElementById("download");
		btn.value = "download";
		btn.disabled = false;
	}
</script>
{{ end }}`
