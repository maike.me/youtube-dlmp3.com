package tmpl

const LayoutHTML = `
<!DOCTYPE html>
<html style="margin: 0;" lang="{{ .Common.Lang }}">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ if .Common.MetaTitle }}{{ .Common.MetaTitle }} - YouTube-DLMP3.com{{ else }}YouTube-DLMP3.com{{ end }}</title>
	<link rel="shortcut icon" type="image/png" href="/static/images/favicon.png"/>
	<link rel="apple-touch-icon" sizes="180x180" href="/static/images/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/static/images/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/static/images/favicons/favicon-16x16.png">
	<link rel="manifest" href="/static/images/favicons/site.webmanifest">
	<link rel="mask-icon" href="/static/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/static/images/favicons/favicon.ico">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-config" content="/static/images/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	{{ if .Common.MetaDescription }}
		<meta name="description" content="{{ .Common.MetaDescription }}">
	{{ else }}
		<meta name="description" content="YouTube-DLMP3.com is a free, open source web application that allows you to download the highest quality audio file of any YouTube video.">
	{{ end }}
	<meta name="keywords" content="YouTube to MP3,YouTube2MP3,youtube-dl,avconv,FFmpeg,download/save YouTube music/songs">
	{{ block "head" . }}{{ end }}
	<style>
	* {
		font-family: 'open_sansregular', Arial, sans-serif;
		color: white;
		line-height: 1.5rem;
	}
	body {
		background-color: #09d;
	}
	b, strong {
		font-family: 'open_sanssemibold', Arial, sans-serif;
		font-weight: normal;
	}
	i, em {
		font-family: 'open_sansitalic', Arial, sans-serif;
		font-style: normal;
	}
	@font-face {
		font-family: 'open_sansregular';
		src: url('/static/fonts/OpenSans-Regular-webfont.eot');
		src: url('/static/fonts/OpenSans-Regular-webfont.eot?#iefix') format('embedded-opentype'),
			 url('/static/fonts/OpenSans-Regular-webfont.woff2') format('woff2'),
			 url('/static/fonts/OpenSans-Regular-webfont.woff') format('woff'),
			 url('/static/fonts/OpenSans-Regular-webfont.ttf') format('truetype'),
			 url('/static/fonts/OpenSans-Regular-webfont.svg#open_sansregular') format('svg');
		font-weight: normal;
		font-style: normal;
	}
	@font-face {
		font-family: 'open_sansitalic';
		src: url('/static/fonts/OpenSans-Italic-webfont.eot');
		src: url('/static/fonts/OpenSans-Italic-webfont.eot?#iefix') format('embedded-opentype'),
			 url('/static/fonts/OpenSans-Italic-webfont.woff2') format('woff2'),
			 url('/static/fonts/OpenSans-Italic-webfont.woff') format('woff'),
			 url('/static/fonts/OpenSans-Italic-webfont.ttf') format('truetype'),
			 url('/static/fonts/OpenSans-Italic-webfont.svg#open_sansitalic') format('svg');
		font-weight: normal;
		font-style: normal;
	}
	@font-face {
		font-family: 'open_sanssemibold';
		src: url('/static/fonts/OpenSans-Semibold-webfont.eot');
		src: url('/static/fonts/OpenSans-Semibold-webfont.eot?#iefix') format('embedded-opentype'),
			 url('/static/fonts/OpenSans-Semibold-webfont.woff2') format('woff2'),
			 url('/static/fonts/OpenSans-Semibold-webfont.woff') format('woff'),
			 url('/static/fonts/OpenSans-Semibold-webfont.ttf') format('truetype'),
			 url('/static/fonts/OpenSans-Semibold-webfont.svg#open_sanssemibold') format('svg');
		font-weight: normal;
		font-style: normal;
	}
	@font-face {
		font-family: 'open_sanssemibold_italic';
		src: url('/static/fonts/OpenSans-SemiboldItalic-webfont.eot');
		src: url('/static/fonts/OpenSans-SemiboldItalic-webfont.eot?#iefix') format('embedded-opentype'),
			 url('/static/fonts/OpenSans-SemiboldItalic-webfont.woff2') format('woff2'),
			 url('/static/fonts/OpenSans-SemiboldItalic-webfont.woff') format('woff'),
			 url('/static/fonts/OpenSans-SemiboldItalic-webfont.ttf') format('truetype'),
			 url('/static/fonts/OpenSans-SemiboldItalic-webfont.svg#open_sanssemibold_italic') format('svg');
		font-weight: normal;
		font-style: normal;
	}
	@media (max-width: 430px) {
    input {
			max-width: 90%;
		}
	}
	</style>
</head>
<body style="margin: 0; height: 100%;">
	<header style="text-align: center; margin: 3rem 0 1.5rem 0;">
		<a href="/" style="font-size: xx-large;">YouTube-DLMP3.com</a>
	</header>
	<div style="margin: 1rem;">
  	{{ block "content" . }}{{ end }}
		{{ if .Common.Flashes }}
		<div>
			{{ range $index, $element := .Common.Flashes }}
			<p style="text-align: center; color: #d04;">{{ $element }}</p>
			{{ end }}
		</div>
		{{ end }}
		<p style="text-align: center; margin-top: 7rem;">
			Requesting same YouTube video repeatedly will make your request slower, not faster.
			<br>If something isn't working then try with another YouTube video, or wait at least 30 seconds.
			<br><br>Downloading copyrighted audio without permisson from the owner is illegal.
			<br>Please be mindful of the audio you download with this service.
		</p>
	</div>
	<footer style="padding: 0 .5rem 0 .5rem; margin-bottom: 1rem;">
		<div style="text-align: center;">
			{{ if eq "en" .Common.Lang }}
			No tracking. No bloat. <a href="https://gitlab.com/maike.me/youtube-dlmp3.com">Open Source</a>.
			{{ end }}
			<br>© Michael Frandsen Copyright, 2018. All rights reserved.
		</div>
	</footer>
	{{ block "scripts" . }}{{ end }}
</body>
</html>`
