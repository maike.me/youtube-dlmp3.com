package templates

import (
	"fmt"
	"html/template"
	"net/http"
	"net/url"

	"github.com/gorilla/csrf"
	"gitlab.com/maike.me/youtube-dlmp3.com/handlers"
)

type CommonData struct {
	Lang            string
	MetaTitle       string
	MetaDescription string
	CSRF            template.HTML
	Flashes         []interface{}
	CurrentURL      template.URL
}

func ReadCommonData(w http.ResponseWriter, r *http.Request) CommonData {
	currentURL := "/"
	if r.URL.Path != "" {
		currentURL = r.URL.Path
		if r.URL.RawQuery != "" {
			currentURL = currentURL + "?" + r.URL.RawQuery
		}
	}
	sess := handlers.GetSession(r)
	flashes := sess.Flashes()
	err := sess.Save(r, w)
	if err != nil {
		fmt.Println(err)
	}
	return CommonData{
		Lang:       "en",
		CSRF:       csrf.TemplateField(r),
		CurrentURL: template.URL(url.QueryEscape(currentURL)),
		Flashes:    flashes,
	}
}
