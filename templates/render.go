package templates

import (
	"html/template"
	"io"

	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/html"
	"gitlab.com/maike.me/youtube-dlmp3.com/templates/tmpl"
)

var tmpls map[string]*template.Template = make(map[string]*template.Template)

func init() {
	layout := minifyHTML(tmpl.LayoutHTML)

	tmpls["index.html"] = template.Must(template.New("layout").Parse(layout))
	template.Must(tmpls["index.html"].New("index").Parse(minifyHTML(tmpl.IndexHTML)))
}

func Render(wr io.Writer, template string, data interface{}) {
	err := tmpls[template].Execute(wr, data)
	if err != nil {
		panic(err)
	}
}

func minifyHTML(input string) string {
	min := minify.New()
	min.Add("text/html", &html.Minifier{})
	str, err := min.String("text/html", input)
	if err != nil {
		panic(err)
	}
	return str
}
